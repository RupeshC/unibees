//
//  RegisterViewController.m
//  UNIBEES
//
//  Created by Rupesh Chakole on 20/07/16.
//  Copyright (c) 2016 Pradeep. All rights reserved.
//

#import "RegisterViewController.h"
#import "ViewController.h"
#import "AboutYouViewController.h"
#import "webservice.h"
#import "AppDelegate.h"
//#include "WToast.h"
#import "MBProgressHUD.h"
#import "FMDatabase.h"

@interface RegisterViewController ()

@end

@implementation RegisterViewController
{
    webservice *web;
    ViewController *toast;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    [self.txt_usrCity setDelegate:self];
    [self.txt_usrEmail setDelegate:self];
    [self.txt_usrName setDelegate:self];
    [self.txt_usrPhoneNo setDelegate:self];
    [self.txt_usrPwd setDelegate:self];
    [self.txt_usrPwdAgain setDelegate:self];
    [self.txt_usrUnivercity setDelegate:self];
    
    toast=[[ViewController alloc] initWithNibName:nil bundle:nil];
    
    self.navigationItem.title=@"Register";
    
    //==== Number Pad Button ===
    UIToolbar* numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
    numberToolbar.barStyle = UIBarStyleBlackTranslucent;
    numberToolbar.items = @[[[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                            [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                            [[UIBarButtonItem alloc]initWithTitle:@"Apply" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)]];
    [numberToolbar sizeToFit];
    _txt_usrPhoneNo.inputAccessoryView = numberToolbar;
    //==========================
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)LeavRegistration:(UIBarButtonItem *)sender {
    
}

- (IBAction)btn_regUser:(id)sender {
    if (_txt_usrName.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Name is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_txt_usrEmail.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Email is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if ([self validateEmailWithString:_txt_usrEmail.text]==0)
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Please enter correct email id!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_txt_usrPwd.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Password is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_txt_usrPwd.text.length<6) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Please enter correct password" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_txt_usrPwdAgain.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Please enter password again!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (![_txt_usrPwd.text isEqualToString:[NSString stringWithFormat:@"%@",_txt_usrPwdAgain.text]]) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Passwords not matches" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
//    else if (_txt_usrPhoneNo.text.length==0) {
//        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
//                                                             message:@"Phone number is Missing!!" delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//        [ErrorAlert show];
//    }
//    else if((_txt_usrPhoneNo.text.length >0) && ([self validateMobileNumber:_txt_usrPhoneNo.text]==0))
//    {
//        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
//                                                             message:@"Phone number should starts from digits 0 or 7 or 9 and have 10 digits!!" delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//        [ErrorAlert show];
//    }
    // In Mobile1, One digit should not repeate 8 times
    else if( (_txt_usrPhoneNo.text.length >0) && ([self validateMobileNumber_digit8times:_txt_usrPhoneNo.text])==0)
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"In phone number, one digit should not repeat 8 times!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if( (_txt_usrPhoneNo.text.length >0) && (([[_txt_usrPhoneNo.text substringFromIndex:1]isEqualToString:@"0"]) && (_txt_usrPhoneNo.text.length==9)))
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Please enter valid phone number!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
//    else if (_txt_usrCity.text.length==0) {
//        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
//                                                             message:@"City is Missing!!" delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//        [ErrorAlert show];
//    }
//    else if (_txt_usrUnivercity.text.length==0) {
//        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
//                                                             message:@"Univercity is Missing!!" delegate:nil
//                                                   cancelButtonTitle:@"OK"
//                                                   otherButtonTitles:nil, nil];
//        [ErrorAlert show];
//    }
    else{
        NSString *USER_TYPE=@"1", *MOBILE=@"0";
        
        if(_txt_usrPhoneNo.text.length==0)
            MOBILE=@"0";
        else
            MOBILE=_txt_usrPhoneNo.text;
        
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"USER_NAME=%@&USER_TYPE=%@&MOBILE=%@&EMAIL_ID=%@&PASSWORD=%@",_txt_usrName.text,USER_TYPE,MOBILE,_txt_usrEmail.text,_txt_usrPwd.text];
        
        
        web=[webservice alloc];
        web.owner=self;
        web.delegate=self;
        
        [web postData:@"registration.php" para:myRequestString];
    }
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField==_txt_usrPhoneNo) {
        if(range.length + range.location > textField.text.length)
        {
            return NO;
        }
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        return newLength <= 11;
    }
    
    return YES;
}

//Code for Avoid the Keyboard to cover the views
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int movementDistance=0;
    if (textField==_txt_usrPwdAgain)
         movementDistance = 10;
    else if (textField==_txt_usrPhoneNo)
        movementDistance = 50;
    else if (textField==_txt_usrCity)
        movementDistance = 85;
    else if (textField==_txt_usrUnivercity)
        movementDistance = 120;


    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

// Number Keyboard button Fuctions
-(void)cancelNumberPad{
    _txt_usrPhoneNo.text=@"";
}

-(void)doneWithNumberPad{
    [_txt_usrPhoneNo resignFirstResponder];
}

// Email Validation
- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}


// Mobile Number validation
- (BOOL) validateMobileNumber: (NSString *) cardNumber {
    NSString *MobileRegex = @"[079][0-9]{10}";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MobileRegex];
    return [cardTest evaluateWithObject:cardNumber];
}

// Mobile number, one digit should not return 8 times
- (Boolean) validateMobileNumber_digit8times: (NSString *) MobNumber {
    int count=0;
    int mob_at_i=0,mob_at_j=0;
    for(int i=0;i<[MobNumber length];i++)
    {
        for(int j=0;j<10;j++)
        {
            mob_at_i=[[MobNumber substringWithRange:NSMakeRange(i,1)] intValue];
            mob_at_j=[[MobNumber substringWithRange:NSMakeRange(j,1)] intValue];
            if(mob_at_i==mob_at_j)
                ++count;
        }
        if(count>=8)
            return false;
        count=0;
    }
    return true;
}


//webservice delegate
- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess{
    if (web)
    {
        web.delegate=nil;
        web=nil;
    }
    
    if (sucess)
    {
        if (result)
        {
            NSLog(@"result %@",result);
            
            if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
            {
                
                [self SetDataFromTbl];
                
                AboutYouViewController *controller = (AboutYouViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"AboutYouViewController"];
                
                controller.EmailID=_txt_usrEmail.text;
                [self.navigationController pushViewController:controller animated:YES];
                
                [self ShowToast:@"User register sucefully."];
            }
            else
            {
                [self ShowToast:@"Something went wrong"];
            }
        }
        else
        {
            [self ShowToast:@"Server Error"];
            
        }
        
    }
    else
    {
        [self ShowToast:@"Server Error"];
    }
}


-(void) ShowToast:(NSString *)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}


-(void)SetDataFromTbl{
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    appDelegate.user=[[user alloc]init];
    
    appDelegate.user.USER_NAME=_txt_usrName.text;
    appDelegate.user.USER_ID=@"";
    appDelegate.user.MOBILE=[NSString stringWithFormat:@"%@",_txt_usrPhoneNo.text] ;
    appDelegate.user.EMAIL_ID=_txt_usrEmail.text;
    appDelegate.emailId=_txt_usrEmail.text;
    appDelegate.userID=@"";
    appDelegate.user.USER_TYPE=@"1";
    appDelegate.user.CITY=[NSString stringWithFormat:@"%@",_txt_usrCity.text] ;
    appDelegate.user.STATE_CODE=@"";
    appDelegate.user.COUNTRY=@"";
    appDelegate.user.UNIVERSITY=[NSString stringWithFormat:@"%@",_txt_usrUnivercity.text] ;
    
    [self InsertIntoTable:@"True" :appDelegate.user.USER_NAME: appDelegate.user.USER_ID:appDelegate.user.EMAIL_ID :appDelegate.user.MOBILE :appDelegate.user.USER_TYPE :appDelegate.user.CITY :appDelegate.user.STATE_CODE :appDelegate.user.COUNTRY :appDelegate.user.UNIVERSITY];
}

-(void) InsertIntoTable:(NSString *) LogedIn_flag:(NSString *) USER_NAME:(NSString *) USER_ID:(NSString *) EMAIL_ID:(NSString *) MOBILE:(NSString *) USER_TYPE:(NSString *) CITY:(NSString *) STATE_CODE:(NSString *) COUNTRY:(NSString *) UNIVERSITY{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"INSERT INTO UsrLoginInfo (LogedIn_flag, USER_NAME, USER_ID, EMAIL_ID, MOBILE, USER_TYPE, CITY, STATE_CODE, COUNTRY, UNIVERSITY) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",LogedIn_flag,USER_NAME, USER_ID, EMAIL_ID, MOBILE, USER_TYPE,CITY,STATE_CODE,COUNTRY,UNIVERSITY , nil];
    [database close];
}


@end
