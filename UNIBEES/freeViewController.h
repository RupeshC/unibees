//
//  freeViewController.h
//  UNIBEES
//
//  Created by Pradeep on 19/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface freeViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)menu:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@end
