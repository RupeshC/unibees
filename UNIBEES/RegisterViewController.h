//
//  RegisterViewController.h
//  UNIBEES
//
//  Created by Rupesh Chakole on 20/07/16.
//  Copyright (c) 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : UIViewController<UITextFieldDelegate,UIScrollViewDelegate,UIAlertViewDelegate>
{
    CGRect rect;
    IBOutlet UIScrollView *myScrollView;
}

@property (strong, nonatomic) IBOutlet UITextField *txt_usrName;
@property (strong, nonatomic) IBOutlet UITextField *txt_usrEmail;
@property (strong, nonatomic) IBOutlet UITextField *txt_usrPwd;
@property (strong, nonatomic) IBOutlet UITextField *txt_usrPhoneNo;
@property (strong, nonatomic) IBOutlet UITextField *txt_usrCity;
@property (strong, nonatomic) IBOutlet UITextField *txt_usrUnivercity;
@property (strong, nonatomic) IBOutlet UITextField *txt_usrPwdAgain;

- (IBAction)btn_regUser:(id)sender;
- (IBAction)btn_back:(id)sender;

@end
