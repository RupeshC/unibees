//
//  AboutYouViewController.m
//  UNIBEES
//
//  Created by Rupesh Chakole on 31/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "AboutYouViewController.h"
#import "HomeViewController.h"
#import "webservice.h"
#import "NIDropDown.h"
#import "AppDelegate.h"
//#import "WToast.h"
#import "MBProgressHUD.h"
#import "FMDatabase.h"

@interface AboutYouViewController ()

@end

@implementation AboutYouViewController{
    NIDropDown *dropDown_abtUniversity,*dropDown_StdntStatus,*dropDown_abtAgeGrp,*dropDown_abtGender;
    NSString *isEarn;
    webservice *web;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    //==== UIButton Border ====
    [self buttonBorder:_uiBtn_abtGender];
    [self buttonBorder:_uiBtn_abtAgeGrp];
    [self buttonBorder:_uiBtn_stdntStatus];
    [self buttonBorder:_uiBtn_abtUniversity];
    
    isEarn=@"no";
}

-(void)buttonBorder:(UIButton *) btn{
    btn.layer.cornerRadius = 10;
//    [[btn layer] setBorderWidth:0.0f];
//    [[btn layer] setBorderColor:[UIColor orangeColor].CGColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) niDropDownDelegateMethod: (NIDropDown *) sender {
    [self rel];
}

-(void)rel{
    // [dropDown release];
    dropDown_abtAgeGrp = nil;
    dropDown_abtGender=nil;
    dropDown_abtUniversity=nil;
    dropDown_StdntStatus=nil;
}

- (IBAction)btn_abtUniversity:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"University of Texas at Dallas",@"Others",nil];
    
    if(dropDown_abtUniversity == nil) {
        CGFloat f = 80;
        dropDown_abtUniversity = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown_abtUniversity.delegate = self;
    }
    else {
        [dropDown_abtUniversity hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)btn_StdntStatus:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Freshman",@"Sophomore",@"Junior",@"Senior",@"Graduate",@"PhD",@"Faculty / Staff",@"Others",nil];
    
    if(dropDown_StdntStatus == nil) {
        CGFloat f = 280;
        dropDown_StdntStatus = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown_StdntStatus.delegate = self;
    }
    else {
        [dropDown_StdntStatus hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)btn_abtAgeGrp:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"16-20",@"20-25",@"25-30",@"30-40",@"40+",nil];
    
    if(dropDown_abtGender == nil) {
        CGFloat f = 200;
        dropDown_abtGender = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown_abtGender.delegate = self;
    }
    else {
        [dropDown_abtGender hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)btn_abtGender:(id)sender {
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:@"Male",@"Female",@"Not specified",nil];
    
    if(dropDown_abtAgeGrp == nil) {
        CGFloat f = 120;
        dropDown_abtAgeGrp = [[NIDropDown alloc]showDropDown:sender :&f :arr :nil :@"down"];
        dropDown_abtAgeGrp.delegate = self;
    }
    else {
        [dropDown_abtAgeGrp hideDropDown:sender];
        [self rel];
    }
}

- (IBAction)btn_earnIncome_yes:(id)sender {
    [self.uiBtn_earnIncome_yes setImage:[UIImage imageNamed:@"radioBtn.png"] forState:UIControlStateNormal];
    [self.uiBtn_earnIncome_no setImage:[UIImage imageNamed:@"unchck_radioBtn.png"] forState:UIControlStateNormal];
    isEarn=@"yes";
}

- (IBAction)btn_earnIncome_no:(id)sender {
    [self.uiBtn_earnIncome_yes setImage:[UIImage imageNamed:@"unchck_radioBtn.png"] forState:UIControlStateNormal];
    [self.uiBtn_earnIncome_no setImage:[UIImage imageNamed:@"radioBtn.png"] forState:UIControlStateNormal];
    isEarn=@"no";
}

- (IBAction)btn_submit:(id)sender {
    
    if (_uiBtn_abtUniversity.titleLabel.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"University is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_uiBtn_stdntStatus.titleLabel.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Student status is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_uiBtn_abtAgeGrp.titleLabel.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Age group is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_uiBtn_abtGender.titleLabel.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!"
                                                             message:@"Gender is missing!!" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else{
        
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"UNIVERSITY=%@&STUDENT_STATUS=%@&AGE_GROUP=%@&GENDER=%@&ETHNICITY=%@&EARNING_INCOME=%@&EMAIL_ID=%@",_uiBtn_abtUniversity.titleLabel.text,_uiBtn_stdntStatus.titleLabel.text,_uiBtn_abtAgeGrp.titleLabel.text,_uiBtn_abtGender.titleLabel.text,@"asian",isEarn,_EmailID];
        
        
        web=[webservice alloc];
        web.owner=self;
        web.delegate=self;
        web.tag=1;
        [web postData:@"adduserdetails.php" para:myRequestString];
    }
    
}

//webservice delegate
- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess{
    if (web.tag==1) {
        
    
    if (web)
    {
        web.delegate=nil;
        web=nil;
    }
    
    if (sucess)
    {
        if (result)
        {
            NSLog(@"result %@",result);
            
            if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
            {
                
                [self onPushNotification];
                
                HomeViewController *controller = (HomeViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
                [self.navigationController pushViewController:controller animated:YES];
                
//                ViewController *controller = (ViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"ViewController"];
//                [self.navigationController pushViewController:controller animated:YES];
            }
            else
            {
                [self ShowToast:@"Something went wrong"];
            }
        }
        else
        {
            [self ShowToast:@"Server Error"];
            
        }
        
    }
    else
    {
        [self ShowToast:@"Server Error"];
    }
    }
}


-(void)onPushNotification
{
    // Checking if app is running iOS 8
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        // Register device for iOS8
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        // Register device for iOS7
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeBadge];
    }
}


-(void) ShowToast:(NSString *)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}


@end
