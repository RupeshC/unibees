




//
//  commanClass.m
//  UNIBEES
//
//  Created by Pradeep on 19/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "commanClass.h"

@implementation commanClass

+(NSDate *)getdate :(NSString *)timpsn
{
    NSString *str=[NSString stringWithFormat:@"%@",timpsn];
    
    if ([str length]>=10)
    {
        
        timpsn=[str substringToIndex:10];
        
    }
    
    double unixTimeStamp =[timpsn doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    return date;
}

+(NSString *)convertTimspanTodate :(NSString *)timpsn
{
    NSString *str=[NSString stringWithFormat:@"%@",timpsn];
   
    if ([str length]>=10)
    {
        
        timpsn=[str substringToIndex:10];

    }
    
    double unixTimeStamp =[timpsn doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDateFormatter *formatter= [[NSDateFormatter alloc] init];
    [formatter setLocale:[NSLocale currentLocale]];
    [formatter setDateFormat:@"MMM dd hh:mm a"];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

+(int)daysDifference :(NSString *)timpsn
{
    NSString *str=[NSString stringWithFormat:@"%@",timpsn];
    
    if ([str length]>=10)
    {
        
        timpsn=[str substringToIndex:10];
        
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    [formatter setLocale:[NSLocale currentLocale]];
    
    double unixTimeStamp =[timpsn doubleValue];
    NSTimeInterval _interval=unixTimeStamp;
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:_interval];
    NSDate *startDate = [NSDate date];

    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    return [components day];
    
    
}
@end
