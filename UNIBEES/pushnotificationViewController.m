//
//  pushnotificationViewController.m
//  UNIBEES
//
//  Created by Kiran on 7/24/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "pushnotificationViewController.h"
#import "AppDelegate.h"
@interface pushnotificationViewController ()

@end

@implementation pushnotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
    // Do any additional setup after loading the view.
    if (self.notificaitonDic)
    {
        NSString *adid=[NSString stringWithFormat:@"%@",[self.notificaitonDic valueForKey:@"ADHOC_ID"]];
        NSString *adtitle=[NSString stringWithFormat:@"%@",[self.notificaitonDic valueForKey:@"ADHOC_TITLE"]];
        NSString *addesp=[NSString stringWithFormat:@"%@",[self.notificaitonDic valueForKey:@"ADHOC_DESCRIPTION"]];
        
        self.ttl.text=adtitle;
        self.textview.text=addesp;
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)menu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)close:(id)sender {
    
     [self.navigationController popViewControllerAnimated:YES];
}
@end
