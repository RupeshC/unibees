//
//  user.h
//  AutoCrash
//
//  Created by Pradeep on 04/05/16.
//  Copyright © 2016 Hitesh. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface user : NSObject

@property (strong, nonatomic) NSString *USER_NAME;
@property (strong, nonatomic) NSString *USER_ID;
@property (strong, nonatomic) NSString *MOBILE;
@property (strong, nonatomic) NSString *EMAIL_ID;
@property (strong, nonatomic) NSString *USER_TYPE;
@property (strong, nonatomic) NSString *CITY;
@property (strong, nonatomic) NSString *STATE_CODE;
@property (strong, nonatomic) NSString *COUNTRY;
@property (strong, nonatomic) NSString *UNIVERSITY;
@property (assign, atomic) BOOL isFacebbok;
@property (strong, nonatomic) NSString *facebookPicId;
@end
