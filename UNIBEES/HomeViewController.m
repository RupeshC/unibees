//
//  HomeViewController.m
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "HomeViewController.h"
#import "VKSideMenu.h"
#import "ProfileViewController.h"
#import "CarbonKit.h"
#import "MyEventViewController.h"
#import "AdHocViewController.h"
#import "AboutViewController.h"
#import "freeViewController.h"
#import "dealViewController.h"
#import "entertmtViewController.h"
#import "addeventViewController.h"
#import "AppDelegate.h"

#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v) ([[[UIDevice currentDevice] systemVersion] compare:(v) options:NSNumericSearch] != NSOrderedAscending)
@interface HomeViewController ()
@property (nonatomic, strong) VKSideMenu *menuLeft;

@end

@implementation HomeViewController
{
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // Init default left-side menu with custom width
    [self.navigationController setNavigationBarHidden:NO];
    self.menuLeft = [[VKSideMenu alloc] initWithWidth:250 andDirection:VKSideMenuDirectionLeftToRight];
    self.menuLeft.dataSource = (id)self;
    self.menuLeft.delegate   = (id)self;
    

    
    items = @[
              @"FREE",
              @"DEALS",
              @"ENTERTAINMENT"
              ];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    
    [self style];


}
-(void)viewWillAppear:(BOOL)animated
{
    int crrnttab=carbonTabSwipeNavigation.currentTabIndex;
    if (crrnttab==2) {
  
        [carbonTabSwipeNavigation setCurrentTabIndex:0 withAnimation:false];
     
   
    }
  
}
- (void)style {
    
    
    carbonTabSwipeNavigation.toolbar.translucent = YES;
    [carbonTabSwipeNavigation setIndicatorColor:[[UIColor blackColor] colorWithAlphaComponent:0.1]];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3 forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3 forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:self.view.frame.size.width/3 forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setBackgroundColor:[UIColor orangeColor]];
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[[UIColor whiteColor] colorWithAlphaComponent:0.8]
                                        font:[UIFont systemFontOfSize:12]];
    [carbonTabSwipeNavigation setSelectedColor:[UIColor whiteColor] font:[UIFont systemFontOfSize:14]];
    
  
}

-(IBAction)buttonMenuLeft:(id)sender
{
    [self.menuLeft show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0:
            return [self.storyboard instantiateViewControllerWithIdentifier: @"freeViewController"];
            
        case 1:
            return [self.storyboard instantiateViewControllerWithIdentifier: @"dealViewController"];
            
        case 2:
            return [self.storyboard instantiateViewControllerWithIdentifier: @"entertmtViewController"];
        default:
            return [self.storyboard instantiateViewControllerWithIdentifier: @"freeViewController"];
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
//    switch (index) {
//        case 0:
//            self.title = @"Home";
//            break;
//        case 1:
//            self.title = @"Hourglass";
//            break;
//        case 2:
//            self.title = @"Premium Badge";
//            break;
//        default:
//            self.title = items[index];
//            break;
//    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:
(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}


#pragma mark - VKSideMenuDataSource
- (IBAction)menu:(id)sender {
    [self.menuLeft show];
}
-(NSInteger)numberOfSectionsInSideMenu:(VKSideMenu *)sideMenu
{
    return sideMenu == self.menuLeft ? 1 : 2;
}

-(NSInteger)sideMenu:(VKSideMenu *)sideMenu numberOfRowsInSection:(NSInteger)section
{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.user.USER_TYPE isEqualToString:@"2"]) {
        return 7;
    }
    else
        return 6;
  
}

-(VKSideMenuItem *)sideMenu:(VKSideMenu *)sideMenu itemForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // This solution is provided for DEMO propose only
    // It's beter to store all items in separate arrays like you do it in your UITableView's. Right?
    VKSideMenuItem *item = [VKSideMenuItem new];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if ([appDelegate.user.USER_TYPE isEqualToString:@"2"]) {
        if (sideMenu == self.menuLeft) // All LEFT menu items
        {
            switch (indexPath.row)
            {
                case 0:
                    item.title = @"User Info";
                    //item.icon  = [UIImage imageNamed:@"logo.png"];
                    break;
                    
                case 1:
                    item.title = @"Profile";
                    item.icon  = [UIImage imageNamed:@"profile.png"];
                    break;
                    
//                case 2:
//                    item.title = @"My Events";
//                    item.icon  = [UIImage imageNamed:@"heart.png"];
//                    break;
                    
                case 2:
                    item.title = @"Today's Events";
                    item.icon  = [UIImage imageNamed:@"todays_events.png"];
                    break;
                    
//                case 4:
//                    item.title = @"My Offers";
//                    item.icon  = [UIImage imageNamed:@"MyOffers.png"];
//                    break;
                    
//                case 5:
//                    item.title = @"My Entertainment";
//                    item.icon  = [UIImage imageNamed:@"Entertainment.png"];
//                    break;
                    
                case 3:
                    item.title = @"Ad-Hoc Notifications";
                    item.icon  = [UIImage imageNamed:@"ic_adhoc.png"];
                    break;
                    
                case 4:
                    item.title = @"About US";
                    item.icon  = [UIImage imageNamed:@"AboutUNIBEES.png"];
                    break;
                    
                case 5:
                    item.title = @"Share  UNIBEES";
                    item.icon  = [UIImage imageNamed:@"share.png"];
                    break;
                    
                case 6:
                    item.title = @"Enter an event";
                    item.icon  = [UIImage imageNamed:@"event.png"];
                    break;
                    
                default:
                    break;
            }
        }
    }
    else{
        if (sideMenu == self.menuLeft) // All LEFT menu items
        {
            switch (indexPath.row)
            {
                case 0:
                    item.title = @"User Info";
                    //item.icon  = [UIImage imageNamed:@"logo.png"];
                    break;
                    
                case 1:
                    item.title = @"Profile";
                    item.icon  = [UIImage imageNamed:@"profile.png"];
                    break;
                    
//                case 2:
//                    item.title = @"My Events";
//                    item.icon  = [UIImage imageNamed:@"heart.png"];
//                    break;
                    
                case 2:
                    item.title = @"Today's Events";
                    item.icon  = [UIImage imageNamed:@"todays_events.png"];
                    break;
                    
//                case 4:
//                    item.title = @"My Offers";
//                    item.icon  = [UIImage imageNamed:@"MyOffers.png"];
//                    break;
                    
//                case 5:
//                    item.title = @"My Entertainment";
//                    item.icon  = [UIImage imageNamed:@"Entertainment.png"];
//                    break;
                    
                case 3:
                    item.title = @"Ad-Hoc Notifications";
                    item.icon  = [UIImage imageNamed:@"ic_adhoc.png"];
                    break;
                    
                case 4:
                    item.title = @"About US";
                    item.icon  = [UIImage imageNamed:@"AboutUNIBEES.png"];
                    break;
                    
                case 5:
                    item.title = @"Share  UNIBEES";
                    item.icon  = [UIImage imageNamed:@"share.png"];
                    break;
                    
                    
                default:
                    break;
            }
        }
    }
    
    return item;
}

#pragma mark - VKSideMenuDelegate

-(void)sideMenu:(VKSideMenu *)sideMenu didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"SideMenu didSelectRow: %@", indexPath);
    
    if (indexPath.row==0)
    {
        
    }
    else if (indexPath.row==1)
    {
        ProfileViewController *controller = (ProfileViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"ProfileViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
//    else if (indexPath.row==2)
//    {
//        MyEventViewController *controller = (MyEventViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"MyEventViewController"];
//        controller.Email_Id=@"My Events";
//        [self.navigationController pushViewController:controller animated:YES];
//    }
    else if (indexPath.row==2)
    {
        MyEventViewController *controller = (MyEventViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"TodaysEventViewController"];
//        controller.eventDetail=@"Today's Events";
        [self.navigationController pushViewController:controller animated:YES];
    }
//    else if (indexPath.row==4)
//    {
//        MyEventViewController *controller = (MyEventViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"MyOffersViewController"];
////        controller.Email_Id=@"My Offers";
//        [self.navigationController pushViewController:controller animated:YES];
//    }
//    else if (indexPath.row==5)
//    {
//        MyEventViewController *controller = (MyEventViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"MyEntertmntViewController"];
//        //        controller.Email_Id=@"My Offers";
//        [self.navigationController pushViewController:controller animated:YES];
//    }
    else if (indexPath.row==3)
    {
        AdHocViewController *controller = (AdHocViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"AdHocViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==4)
    {
        AboutViewController *controller = (AboutViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"AboutViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (indexPath.row==5)
    {
        NSMutableArray *sharingItems = [NSMutableArray new];
        
        [sharingItems addObject:[NSString stringWithFormat:@"Checkout all the free giveaways, deals and entertainment happening in and around your university campus."]];
        
        [sharingItems addObject:[NSString stringWithFormat:@"\n"]];
        
        
        [sharingItems addObject:[NSString stringWithFormat:@"Download here-"]];
        
        [sharingItems addObject:[NSString stringWithFormat:@"\n"]];
        [sharingItems addObject:[NSString stringWithFormat:@"Android-"]];
        [sharingItems addObject:[NSString stringWithFormat:@"\n"]];
        [sharingItems addObject:[NSString stringWithFormat:@"https://play.google.com/store/apps/details?id=com.digit64.unibees"]];
        
        
        [sharingItems addObject:[NSString stringWithFormat:@"\n"]];
        [sharingItems addObject:[NSString stringWithFormat:@"iPhone-"]];
        [sharingItems addObject:[NSString stringWithFormat:@"\n"]];
        [sharingItems addObject:[NSString stringWithFormat:@"https://itunes.apple.com/app/id1140271589"]];
        
        
        UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:sharingItems applicationActivities:nil];
        [self presentViewController:activityController animated:YES completion:nil];
    }
    else{
        
        addeventViewController *controller = (addeventViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"addeventViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }

}

-(void)sideMenuDidShow:(VKSideMenu *)sideMenu
{
    NSLog(@"%@ VKSideMenue did show", sideMenu == self.menuLeft ? @"LEFT" : @"RIGHT");
}

-(void)sideMenuDidHide:(VKSideMenu *)sideMenu
{
    NSLog(@"%@ VKSideMenue did hide", sideMenu == self.menuLeft ? @"LEFT" : @"RIGHT");
}

-(NSString *)sideMenu:(VKSideMenu *)sideMenu titleForHeaderInSection:(NSInteger)section
{
  
        return nil;
   
}



@end
