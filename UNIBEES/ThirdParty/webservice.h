//
//  webservice.h
//  AutoCrash
//
//  Created by Pradeep on 04/05/16.
//  Copyright © 2016 Hitesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol webdelegate

- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess;

@end

@interface webservice : NSObject
@property (nonatomic, assign) int tag;
@property (strong, nonatomic)  UIViewController *owner;
@property (nonatomic, strong) id<webdelegate> delegate;
-(void)getData : (NSString *)url;
-(void)postData : (NSString *)url para:(NSString *)para;
@end
