   

//
//  webservice.m
//  AutoCrash
//
//  Created by Pradeep on 04/05/16.
//  Copyright © 2016 Hitesh. All rights reserved.
//

#import "webservice.h"
#import "MBProgressHUD.h"


@implementation webservice


-(void)getData : (NSString *)url
{
    
   
    NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseurl,url]]];
    
    [request setHTTPMethod: @"GET"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
   
    
    
    
    [MBProgressHUD showHUDAddedTo:self.owner.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSError *err;
        NSURLResponse *response;
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.owner.view animated:YES];
            
            if (err)
            {
                [self.delegate finishLoading:nil sucess:false];
            }
            else
            {
                NSError *jsonError;
                NSDictionary *json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                     options:NSJSONReadingMutableContainers
                                                                       error:&jsonError];
                if (jsonError)
                {
                    if (self.delegate)
                    {
                        
                        [self.delegate finishLoading:nil sucess:false];
                    }
                }
                else
                {
                    if (self.delegate)
                    {
                        [self.delegate finishLoading:json sucess:true];
                    }
                }
                
            }
        });
    });
}
-(void)postData : (NSString *)url para:(NSString *)para
{
    //NSString *myRequestString = [[NSString alloc] initWithFormat:@"birthday=jan 12 2016&emailid=hitaesh@gmail.com&password=1234&username=hiteshas"];
    
    NSData *myRequestData = [ NSData dataWithBytes: [ para UTF8String ] length: [ para length ] ];
    NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseurl,url]]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    
    
    
    [MBProgressHUD showHUDAddedTo:self.owner.view animated:YES];
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_LOW, 0), ^{
        NSError *err;
        NSURLResponse *response;
        NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse:&response error:&err];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [MBProgressHUD hideHUDForView:self.owner.view animated:YES];
            
            
            
            if (err)
            {
                [self.delegate finishLoading:nil sucess:false];
            }
            else
            {
                
                NSString* newStr = [NSString stringWithUTF8String:[returnData bytes]];
                
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:returnData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
                if (jsonError)
                {
                    if (self.delegate)
                    {
                    
                    [self.delegate finishLoading:nil sucess:false];
                        }
                }
                else
                {
                    if (self.delegate)
                    {
                    [self.delegate finishLoading:json sucess:true];
                    }
                }
            
            }
        });
    });
}




@end
