//
//  TodaysEventViewController.h
//  UNIBEES
//
//  Created by Rupesh Chakole on 22/07/16.
//  Copyright (c) 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodaysEventViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
- (IBAction)menu:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *tbl_todaysEvnt;
@property (weak, nonatomic) IBOutlet UILabel *mssgeLbl;

@end
