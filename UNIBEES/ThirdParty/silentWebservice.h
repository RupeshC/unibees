//
//  silentWebservice.h
//  UNIBEES
//
//  Created by Pradeep on 19/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol silentWebdelegate

- (void)finishsilentLoading:(NSDictionary *)result sucess:(BOOL)sucess;

@end

@interface silentWebservice : NSObject
@property (nonatomic, assign) int tag;
@property (nonatomic, strong) id<silentWebdelegate> delegate;
-(void)postData : (NSString *)url para:(NSString *)para;
@end
