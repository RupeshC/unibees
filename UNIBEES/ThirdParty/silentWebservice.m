


//
//  silentWebservice.m
//  UNIBEES
//
//  Created by Pradeep on 19/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "silentWebservice.h"

@implementation silentWebservice
-(void)postData : (NSString *)url para:(NSString *)para
{
    //NSString *myRequestString = [[NSString alloc] initWithFormat:@"birthday=jan 12 2016&emailid=hitaesh@gmail.com&password=1234&username=hiteshas"];
    
    NSData *myRequestData = [ NSData dataWithBytes: [ para UTF8String ] length: [ para length ] ];
    NSMutableURLRequest *request = [ [ NSMutableURLRequest alloc ] initWithURL: [ NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseurl,url]]];
    
    [request setHTTPMethod: @"POST"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    [request setHTTPBody: myRequestData];
    
    
    
    NSOperationQueue *mainQueue = [[NSOperationQueue alloc] init];
    [mainQueue setMaxConcurrentOperationCount:5];
    
    
    [NSURLConnection sendAsynchronousRequest:request queue:mainQueue completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error) {
        NSHTTPURLResponse *urlResponse = (NSHTTPURLResponse *)response;
        if (!error) {
            
            NSError *jsonError;
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:responseData
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&jsonError];
            if (jsonError)
            {
                if (self.delegate)
                {
                    
                    [self.delegate finishsilentLoading:nil sucess:false];
                }
            }
            else
            {
                if (self.delegate)
                {
                    [self.delegate finishsilentLoading:json sucess:true];
                }
            }

        }
        else {
            if (self.delegate)
            {
                [self.delegate finishsilentLoading:nil sucess:true];
            }

        }
    }];
    
    
    
}
@end
