
//
//  ProfileViewController.m
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "ProfileViewController.h"
#import "AppDelegate.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <FacebookSDK/FacebookSDK.h>
#import "FMDatabase.h"
@interface ProfileViewController ()

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
       // [self setTitle:@"Profile"];
    

    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    if (appDelegate.user)
    {
        if (appDelegate.user.USER_NAME) {
            self.name.text=[NSString stringWithFormat:@"%@",appDelegate.user.USER_NAME];
        }
        if (appDelegate.user.EMAIL_ID) {
            self.email.text=[NSString stringWithFormat:@"%@",appDelegate.user.EMAIL_ID];
        }
        
        
    }
    
    if (appDelegate.user.isFacebbok)
    {
        
        NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", appDelegate.user.facebookPicId]];
        [self.image setImageWithURL:pictureURL placeholderImage:[UIImage imageNamed:@"placeholder.jpg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
         
    }
   
}
-(void)viewDidAppear:(BOOL)animated
{
    self.image.layer.cornerRadius=self.image.frame.size.height/2;
    self.image.clipsToBounds=true;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) DeleteLoginData{
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [documentPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"DELETE FROM UsrLoginInfo", nil];
    [database close];
}

- (IBAction)menu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)singout:(id)sender {
    
    [self DeleteLoginData];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

   [[FBSession activeSession] closeAndClearTokenInformation];
//    [FBSession.activeSession close];
//    [FBSession setActiveSession:nil];
//    [self fbDidLogout];
    appDelegate.user=nil;
    [self.navigationController popToRootViewControllerAnimated:YES];
}


- (void)fbDidLogout
{
    NSLog(@"Logged out of facebook");
    NSHTTPCookie *cookie;
    NSHTTPCookieStorage *storage = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    for (cookie in [storage cookies])
    {
        NSString* domainName = [cookie domain];
        NSRange domainRange = [domainName rangeOfString:@"facebook"];
        if(domainRange.length > 0)
        {
            [storage deleteCookie:cookie];
        }
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
