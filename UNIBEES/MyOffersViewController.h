//
//  MyOffersViewController.h
//  UNIBEES
//
//  Created by Rupesh Chakole on 23/07/16.
//  Copyright (c) 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOffersViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *lbl_mssage;
@property (weak, nonatomic) IBOutlet UITableView *tbl_MyOfferLst;
- (IBAction)menu:(id)sender;

@end
