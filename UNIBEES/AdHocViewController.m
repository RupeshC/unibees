


//
//  AdHocViewController.m
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "AdHocViewController.h"
#import "AppDelegate.h"
#import "addeventViewController.h"
#import "FMDatabase.h"
#import "pushnotificationViewController.h"
#import "PushNtfcatnTableViewCell.h"
@interface AdHocViewController ()

@end

@implementation AdHocViewController
{
    NSMutableArray *adhocArrya;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
  //  [self setTitle:@"Notifications"];
 
    self.messageLbl.text=@"All the spontaneous events will be coming here, hold on!";
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *USER_TYPE=appDelegate.user.USER_TYPE;
    
    self.tableView.estimatedRowHeight = 201.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
   
    //Cheeck user type to add event
    if ([appDelegate.user.USER_ID isEqualToString:@"2"])//([USER_TYPE isEqualToString:@"1"])
    {
        self.addevnet.hidden=false;
        self.addevnet.enabled=true;
        
    }
    else
    {
        self.addevnet.hidden=true;
        self.addevnet.enabled=false;
    }
    adhocArrya=[[NSMutableArray alloc]init];
    
}
-(void)viewDidAppear:(BOOL)animated
{
    adhocArrya=[[NSMutableArray alloc]init];
    [self GetDataFromTbl];
    [self.tableView reloadData];
}

-(void)GetDataFromTbl
{
    
  
    int i=0;
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    FMResultSet *results = [database executeQuery:@"SELECT * FROM adhocnotification"];

    while([results next]) {
    
        NSString *adid=[NSString stringWithFormat:@"%@",[results stringForColumn:@"ADHOC_ID"]];
        NSString *adtitle=[NSString stringWithFormat:@"%@",[results stringForColumn:@"ADHOC_TITLE"]];
        NSString *addesp=[NSString stringWithFormat:@"%@",[results stringForColumn:@"ADHOC_DESCRIPTION"]];
        NSString *adtime=[NSString stringWithFormat:@"%@",[results stringForColumn:@"ADHOC_TIME"]];

        
//        NSString *adid=[NSString stringWithFormat:@"123"];
//        NSString *adtitle=[NSString stringWithFormat:@"rupesh"];
//        NSString *addesp=[NSString stringWithFormat:@"Demo"];
//        NSString *adtime=[NSString stringWithFormat:@"0000"];
    
        NSDictionary *dict=@{@"ADHOC_ID": adid,
                             @"ADHOC_TITLE":adtitle,
                             @"ADHOC_DESCRIPTION":addesp,
                             @"ADHOC_TIME":adtime
                             };
        if (dict)
        {
            [adhocArrya addObject:dict];
        }
        i++;
    }
    [database close];
    
    if (i==0) {
        _tableView.hidden=true;
    }
    else
        _tableView.hidden=false;
}


#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    if ([adhocArrya count]) {
        self.messageLbl.text=@"";
    }
    return [adhocArrya count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"SimpleTableItem";
    
//    UITableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
//    
//    if (cell == nil) {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
//    }
    
    NSDictionary *adhocDic=[adhocArrya objectAtIndex:indexPath.row];
//     NSString *adtitle=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_TITLE"]];
//    cell.textLabel.text=[NSString stringWithFormat:@"%@",adtitle];
//    return cell;
    
    
    PushNtfcatnTableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"PushNtfcatnTableViewCell"];
    
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"PushNotification_tableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor=[UIColor groupTableViewBackgroundColor];
    NSDictionary *dic=[adhocArrya objectAtIndex:indexPath.row];
    cell.lbl_pushTitle.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_TITLE"]];
    cell.lbl_pushDesc.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_DESCRIPTION"]];
    cell.lbl_pushTime.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_TIME"]];
    cell.lbl_pushAdhocID.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_ID"]];
    
    return cell;
    
    
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *adhocDic=[adhocArrya objectAtIndex:indexPath.row];
     
        pushnotificationViewController *controller = (pushnotificationViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"pushnotificationViewController"];
        if (adhocDic) {
            controller.notificaitonDic=[[NSMutableDictionary alloc]initWithDictionary:adhocDic];
        }
    
    [self.navigationController pushViewController:controller animated:YES];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)menu:(id)sender
{
[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addevnetAction:(id)sender
{
    addeventViewController *controller = (addeventViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"addeventViewController"];
    [self.navigationController pushViewController:controller animated:YES];
}

@end
