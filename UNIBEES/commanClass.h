//
//  commanClass.h
//  UNIBEES
//
//  Created by Pradeep on 19/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface commanClass : NSObject
+(NSString *)convertTimspanTodate :(NSString *)timpsn;
+(int)daysDifference :(NSString *)timpsn;
+(NSDate *)getdate :(NSString *)timpsn;
@end
