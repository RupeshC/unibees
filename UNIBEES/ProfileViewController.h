//
//  ProfileViewController.h
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileViewController : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *email;
- (IBAction)menu:(id)sender;
- (IBAction)singout:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *image;
@end
