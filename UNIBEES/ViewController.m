//
//  ViewController.m
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "ViewController.h"
#import "HomeViewController.h"
#import "RegisterViewController.h"
#import "AboutYouViewController.h"
#import "AppDelegate.h"
#import "webservice.h"
#import "MBProgressHUD.h"
#import "FMDatabase.h"
#import "AboutYouViewController.h"

@interface ViewController ()

-(void)toggleHiddenState:(BOOL)shouldHide;
@end

@implementation ViewController
{
    webservice *web;
    NSDictionary *fbuserdetails;
    NSString *LogedIn_flag;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.navigationController setNavigationBarHidden:YES];
    [self.email setDelegate:self];
    [self.pass setDelegate:self];
    self.email.text=@"";
    self.pass.text=@"";
    NSArray *_fbPermissions =    @[@"email",@"public_profile"];
    self.FBLoginBtn.readPermissions = _fbPermissions;
    
    //Rounded Corner
    _UIBtn_regByMail.layer.cornerRadius = 5;
    _UIBtn_regByMail.clipsToBounds = YES;
    _UILoginBtn.layer.cornerRadius = 5;
    _UILoginBtn.clipsToBounds = YES;
    
    
    [self.FBLoginBtn setDelegate:self];
    [self.view addSubview:_FBLoginBtn];
    
    
    [self CreateTable];
    
    if ([[self GetDataFromTbl] isEqualToString:@"True"]) {
        HomeViewController *controller = (HomeViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
        [self.navigationController pushViewController:controller animated:YES];
    }
    
}
-(void)viewWillAppear:(BOOL)animated
{
 [self.navigationController setNavigationBarHidden:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    
    if (_email.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Email"
                                                             message:@"Please enter valid email" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if ([self validateEmailWithString:_email.text]==0)
    {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Email"
                                                             message:@"Please enter valid email" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else if (_pass.text.length==0) {
        UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"Password"
                                                             message:@"Password is missing" delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil, nil];
        [ErrorAlert show];
    }
    else{
        NSString *eamild=self.email.text;
        NSString *pass=self.pass.text;
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@&PASSWORD=%@",eamild,pass];
        
        
        web=[webservice alloc];
        web.owner=self;
        web.tag=1;
        web.delegate=self;
        
        [web postData:@"login.php" para:myRequestString];
    }
    
    
}

-(void)onPushNotification
{
    // Checking if app is running iOS 8
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        // Register device for iOS8
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        // Register device for iOS7
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeBadge];
    }
}

#pragma mark - Private method implementation

-(void)toggleHiddenState:(BOOL)shouldHide{
}


-(void) CreateTable{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS UsrLoginInfo (id INTEGER  PRIMARY KEY AUTOINCREMENT NOT NULL,LogedIn_flag TEXT DEFAULT NULL,USER_NAME TEXT DEFAULT NULL, USER_ID TEXT DEFAULT NULL ,EMAIL_ID TEXT DEFAULT NULL,MOBILE TEXT DEFAULT NULL,USER_TYPE TEXT DEFAULT NULL, CITY TEXT DEFAULT NULL, STATE_CODE TEXT DEFAULT NULL, COUNTRY TEXT DEFAULT NULL,UNIVERSITY TEXT DEFAULT NULL)"];
    [database close];
}

-(void) InsertIntoTable:(NSString *) LogedIn_flag:(NSString *) USER_NAME:(NSString *) USER_ID:(NSString *) EMAIL_ID:(NSString *) MOBILE:(NSString *) USER_TYPE:(NSString *) CITY:(NSString *) STATE_CODE:(NSString *) COUNTRY:(NSString *) UNIVERSITY{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"INSERT INTO UsrLoginInfo (LogedIn_flag, USER_NAME, USER_ID, EMAIL_ID, MOBILE, USER_TYPE, CITY, STATE_CODE, COUNTRY, UNIVERSITY) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",LogedIn_flag,USER_NAME, USER_ID, EMAIL_ID, MOBILE, USER_TYPE,CITY,STATE_CODE,COUNTRY,UNIVERSITY , nil];
    [database close];
}

-(NSString *)GetDataFromTbl{
    
    LogedIn_flag=@"False";
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    FMResultSet *results = [database executeQuery:@"SELECT * FROM UsrLoginInfo"];
    // Or with variables FMResultSet *results = [database executeQuery:@"SELECT * from tableName where fieldName= ?",[NSString stringWithFormat:@"%@", variableName]];
    while([results next]) {
        
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        appDelegate.user=[[user alloc]init];
        
        LogedIn_flag = [results stringForColumn:@"LogedIn_flag"];
        appDelegate.user.USER_NAME=[results stringForColumn:@"USER_NAME"];
        appDelegate.user.USER_ID=[results stringForColumn:@"USER_ID"];
        appDelegate.user.MOBILE=[results stringForColumn:@"MOBILE"];
        appDelegate.user.EMAIL_ID=[results stringForColumn:@"EMAIL_ID"];
        appDelegate.emailId=[results stringForColumn:@"EMAIL_ID"];
        appDelegate.userID=[results stringForColumn:@"USER_ID"];
        appDelegate.user.USER_TYPE=[results stringForColumn:@"USER_TYPE"];
        appDelegate.user.CITY=[results stringForColumn:@"CITY"];
        appDelegate.user.STATE_CODE=[results stringForColumn:@"STATE_CODE"];
        appDelegate.user.COUNTRY=[results stringForColumn:@"COUNTRY"];
        appDelegate.user.UNIVERSITY=[results stringForColumn:@"UNIVERSITY"];
        appDelegate.user.USER_TYPE=[results stringForColumn:@"USER_TYPE"];
    }
    [database close];
    
    return LogedIn_flag;
}


#pragma mark - FBLoginView Delegate method implementation

-(void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    
    [self toggleHiddenState:NO];
}


-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)userfb{
    
    NSLog(@"User Name=%@",userfb.name);
    NSLog(@"User Email=%@",[userfb objectForKey:@"email"]);
    
  
 
  
    if (FBSession.activeSession.isOpen)
    {
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
        [FBRequestConnection startWithGraphPath:@"me" parameters:[NSDictionary dictionaryWithObject:@"id,name,email" forKey:@"fields"] HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection, id result, NSError *error)
         {
             {
                 if (!error)
                 {
                     if ([result isKindOfClass:[NSDictionary class]])
                     {
                         
                         NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                         
                         NSString *isLoggedIn = @"isLoggedIn";
                         
                         const NSInteger isLoggedIn_value = 1;
                         [preferences setInteger:isLoggedIn_value forKey:isLoggedIn];
                         
                         //  Save to disk
                         const BOOL didSave = [preferences synchronize];
                         
                         if (!didSave)
                         {
                             NSLog(@"Login save");
                         }
                         [MBProgressHUD hideHUDForView:self.view animated:YES];
                         fbuserdetails=result;
                        
                         NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@&FACEBOOK_USER_ID=%@",[result objectForKey:@"email"],[result objectForKey:@"id"]];
                         
                         
                         web=[webservice alloc];
                         web.owner=self;
                         web.tag=2;
                         web.delegate=self;
                         
                         [web postData:@"login.php" para:myRequestString];
              
                     }
                 }
             }
         }];
    }

    
   
}

-(void)loginView:(FBLoginView *)loginView handleError:(NSError *)error{
      [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    NSLog(@"Error at FBLogin=%@", [error localizedDescription]);
}


-(void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView{
    
    NSLog(@"You are logged out");
}


#pragma mark - webservice delegate

//webservice delegate
- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess{
    if (web.tag==1) {
    
    if (web)
    {
        web.delegate=nil;
        web=nil;
        
    }
    
    if (sucess)
    {
        if (result)
        {
            NSLog(@"result %@",result);
            
            if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
            {
              
                
                AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                
                appDelegate.user=[[user alloc]init];
                
                
                appDelegate.user.USER_NAME=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_NAME"]];
                appDelegate.user.USER_ID=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_ID"]];
                appDelegate.user.MOBILE=[NSString stringWithFormat:@"%@",[result objectForKey:@"MOBILE"]];
                appDelegate.user.EMAIL_ID=[NSString stringWithFormat:@"%@",[result objectForKey:@"EMAIL_ID"]];
                
                
                appDelegate.emailId=[NSString stringWithFormat:@"%@",[result objectForKey:@"EMAIL_ID"]];
                
                appDelegate.userID=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_ID"]];
                
                appDelegate.user.USER_TYPE=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_TYPE"]];
                appDelegate.user.CITY=[NSString stringWithFormat:@"%@",[result objectForKey:@"CITY"]];
                appDelegate.user.STATE_CODE=[NSString stringWithFormat:@"%@",[result objectForKey:@"STATE_CODE"]];
                appDelegate.user.COUNTRY=[NSString stringWithFormat:@"%@",[result objectForKey:@"COUNTRY"]];
                appDelegate.user.UNIVERSITY=[NSString stringWithFormat:@"%@",[result objectForKey:@"UNIVERSITY"]];
                appDelegate.user.USER_TYPE=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_TYPE"]];
                appDelegate.user.isFacebbok=false;
                
                NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                
                NSString *isLoggedIn = @"isLoggedIn";
                
                const NSInteger isLoggedIn_value = 1;
                [preferences setInteger:isLoggedIn_value forKey:isLoggedIn];
                
                //  Save to disk
                const BOOL didSave = [preferences synchronize];
                
                if (!didSave)
                {
                    NSLog(@"Login save");
                }
                [self onPushNotification];
                
                // Inserting values in db
                [self InsertIntoTable:@"True" :appDelegate.user.USER_NAME: appDelegate.user.USER_ID :appDelegate.user.EMAIL_ID :appDelegate.user.MOBILE :appDelegate.user.USER_TYPE :appDelegate.user.CITY :appDelegate.user.STATE_CODE :appDelegate.user.COUNTRY :appDelegate.user.UNIVERSITY];
                
                HomeViewController *controller = (HomeViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
                [self.navigationController pushViewController:controller animated:YES];
            }
            else
            {
//                [WToast showWithText:@"Something Wrong"];
                [self ShowToast:@"Something went wrong"];
            }
            
            
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
            
        }
        
    }
    else
    {
//        [WToast showWithText:@"Server Error"];
        [self ShowToast:@"Server Error"];
    }
        
    }
    
    else if (web.tag==2) {
        
        if (web)
        {
            web.delegate=nil;
            web=nil;
            
        }
        
        if (sucess)
        {
            if (result)
            {
                NSLog(@"result %@",result);
                
                if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
                {
                    
                    
                    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                    
                    appDelegate.user=[[user alloc]init];
                    
                    
                    appDelegate.user.USER_NAME=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_NAME"]];
                    appDelegate.user.USER_ID=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_ID"]];
                    appDelegate.user.MOBILE=[NSString stringWithFormat:@"%@",[result objectForKey:@"MOBILE"]];
                    appDelegate.user.EMAIL_ID=[NSString stringWithFormat:@"%@",[result objectForKey:@"EMAIL_ID"]];
                    
                    
                    appDelegate.emailId=[NSString stringWithFormat:@"%@",[result objectForKey:@"EMAIL_ID"]];
                    
                    appDelegate.userID=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_ID"]];
                    
                    appDelegate.user.USER_TYPE=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_TYPE"]];
                    appDelegate.user.CITY=[NSString stringWithFormat:@"%@",[result objectForKey:@"CITY"]];
                    appDelegate.user.STATE_CODE=[NSString stringWithFormat:@"%@",[result objectForKey:@"STATE_CODE"]];
                    appDelegate.user.COUNTRY=[NSString stringWithFormat:@"%@",[result objectForKey:@"COUNTRY"]];
                    appDelegate.user.UNIVERSITY=[NSString stringWithFormat:@"%@",[result objectForKey:@"UNIVERSITY"]];
                    appDelegate.user.USER_TYPE=[NSString stringWithFormat:@"%@",[result objectForKey:@"USER_TYPE"]];
                    appDelegate.user.facebookPicId=[fbuserdetails objectForKey:@"id"];
                    appDelegate.user.isFacebbok=true;
                    appDelegate.FBLoginBtn=self.FBLoginBtn;
                    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                    
                    NSString *isLoggedIn = @"isLoggedIn";
                    
                    const NSInteger isLoggedIn_value = 1;
                    [preferences setInteger:isLoggedIn_value forKey:isLoggedIn];
                    
                    //  Save to disk
                    const BOOL didSave = [preferences synchronize];
                    
                    if (!didSave)
                    {
                        NSLog(@"Login save");
                    }
                    [self onPushNotification];
                    
                    // Inserting values in db
                    [self InsertIntoTable:@"True" :appDelegate.user.USER_NAME: appDelegate.user.USER_ID:appDelegate.user.EMAIL_ID :appDelegate.user.MOBILE :appDelegate.user.USER_TYPE :appDelegate.user.CITY :appDelegate.user.STATE_CODE :appDelegate.user.COUNTRY :appDelegate.user.UNIVERSITY];
                    
                    
                    HomeViewController *controller = (HomeViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
                    [self.navigationController pushViewController:controller animated:YES];
                }
                else
                {
                    NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@&FACEBOOK_USER_ID=%@&USER_NAME=%@",[fbuserdetails objectForKey:@"email"],[fbuserdetails objectForKey:@"id"],[fbuserdetails objectForKey:@"name"]];
                    
                    web=[webservice alloc];
                    web.owner=self;
                    web.tag=3;
                    web.delegate=self;
                    
                    [web postData:@"facebookreg.php" para:myRequestString];
                  
                    
                }
                
                
                
            }
            else
            {
                [self ShowToast:@"Server Error"];
                
            }
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
        }
        
    }
    
    else if (web.tag==3) {
        
        if (web)
        {
            web.delegate=nil;
            web=nil;
            
        }
        
        if (sucess)
        {
            if (result)
            {
                NSLog(@"result %@",result);
                
                if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
                {
                    NSArray *userarray=[result objectForKey:@"USER"];
                    
                    if ([userarray count]) {
                       NSDictionary *dic=[userarray objectAtIndex:0];

                        
                        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
                        
                        appDelegate.user=[[user alloc]init];
                        
                        
                        appDelegate.user.USER_NAME=[NSString stringWithFormat:@"%@",[dic objectForKey:@"USER_NAME"]];
                        appDelegate.user.USER_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"USER_ID"]];
                        appDelegate.user.MOBILE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"MOBILE"]];
                        appDelegate.user.EMAIL_ID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"EMAIL_ADDRESS"]];
                        appDelegate.emailId=[NSString stringWithFormat:@"%@",[dic objectForKey:@"EMAIL_ID"]];
                        
                        appDelegate.userID=[NSString stringWithFormat:@"%@",[dic objectForKey:@"USER_ID"]];
                        
                        appDelegate.user.USER_TYPE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"USER_TYPE"]];
                        appDelegate.user.CITY=[NSString stringWithFormat:@"%@",[dic objectForKey:@"CITY"]];
                        appDelegate.user.STATE_CODE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"STATE_CODE"]];
                        appDelegate.user.COUNTRY=[NSString stringWithFormat:@"%@",[dic objectForKey:@"COUNTRY"]];
                        appDelegate.user.UNIVERSITY=[NSString stringWithFormat:@"%@",[dic objectForKey:@"UNIVERSITY"]];
                        appDelegate.user.USER_TYPE=[NSString stringWithFormat:@"%@",[dic objectForKey:@"USER_TYPE"]];
                        appDelegate.user.facebookPicId=[fbuserdetails objectForKey:@"id"];
                        appDelegate.user.isFacebbok=true;
                        appDelegate.FBLoginBtn=self.FBLoginBtn;
                        
                        NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];
                        
                        NSString *isLoggedIn = @"isLoggedIn";
                        
                        const NSInteger isLoggedIn_value = 1;
                        [preferences setInteger:isLoggedIn_value forKey:isLoggedIn];
                        
                        //  Save to disk
                        const BOOL didSave = [preferences synchronize];
                        
                        if (!didSave)
                        {
                            NSLog(@"Login save");
                        }
                        [self onPushNotification];
                        
                        // Inserting values in db
                        [self InsertIntoTable:@"True" :appDelegate.user.USER_NAME: appDelegate.user.USER_ID :appDelegate.user.EMAIL_ID :appDelegate.user.MOBILE :appDelegate.user.USER_TYPE :appDelegate.user.CITY :appDelegate.user.STATE_CODE :appDelegate.user.COUNTRY :appDelegate.user.UNIVERSITY];
                        
                        
                        AboutYouViewController *controller = (AboutYouViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
                        controller.EmailID=appDelegate.user.EMAIL_ID;
                        [self.navigationController pushViewController:controller animated:YES];
                    }
                    
                    
                }
                else
                {
                     [[FBSession activeSession] closeAndClearTokenInformation];
//                     [WToast showWithText:@"Something Wrong"];
                    [self ShowToast:@"Something Wrong"];
                }
                
                
                
            }
            else
            {
                [self ShowToast:@"Server Error"];
            }
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
        }
        
    }
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    return YES;
}

//Code for Avoid the Keyboard to cover the views
- (void) animateTextField: (UITextField*) textField up: (BOOL) up
{
    int movementDistance = 10;
    const float movementDuration = 0.3f; // tweak as needed
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"anim" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    self.view.frame = CGRectOffset(self.view.frame, 0, movement);
    [UIView commitAnimations];
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self animateTextField: textField up: YES];
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

// This method is called once we complete editing
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self animateTextField: textField up: NO];
}

- (IBAction)btn_RegisterByMail:(id)sender {
    RegisterViewController *controller = (RegisterViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"RegisterViewController"];

//    AboutYouViewController *controller = (AboutYouViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"AboutYouViewController"];
    //
    [self.navigationController pushViewController:controller animated:YES];
}

- (BOOL)validateEmailWithString:(NSString*)checkString
{
    BOOL stricterFilter = NO;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

-(void) ShowToast:(NSString *)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}


@end
