//
//  PushNtfcatnTableViewCell.h
//  UNIBEES
//
//  Created by Rupesh Chakole on 11/08/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PushNtfcatnTableViewCell : UITableViewCell


@property (strong, nonatomic) IBOutlet UILabel *lbl_pushTitle;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pushDesc;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pushTime;
@property (strong, nonatomic) IBOutlet UILabel *lbl_pushAdhocID;

/*cell.lbl_pushTitle.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_TITLE"]];
 cell.lbl_pushDesc.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_DESCRIPTION"]];
 cell.lbl_pushTime.text=[NSString stringWithFormat:@"%@",[adhocDic valueForKey:@"ADHOC_TIME"]];
 cell.lbl_pushAdhocID.*/
@end
