//
//  AdHocViewController.h
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AdHocViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIButton *addevnet;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
- (IBAction)menu:(id)sender;
- (IBAction)addevnetAction:(id)sender;
@end
