//
//  MyEventViewController.h
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyEventViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString  *Email_Id;
- (IBAction)menu:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *messageLbl;
@end
