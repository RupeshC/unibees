//
//  AboutYouViewController.h
//  UNIBEES
//
//  Created by Rupesh Chakole on 31/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutYouViewController : UIViewController<UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
{
    IBOutlet UIScrollView *myScrollView;
}

@property(strong,nonatomic) NSString *EmailID;

@property (strong, nonatomic) IBOutlet UIButton *uiBtn_abtUniversity;
@property (strong, nonatomic) IBOutlet UIButton *uiBtn_stdntStatus;
@property (strong, nonatomic) IBOutlet UIButton *uiBtn_abtAgeGrp;
@property (strong, nonatomic) IBOutlet UIButton *uiBtn_abtGender;

@property (strong, nonatomic) IBOutlet UIButton *uiBtn_earnIncome_yes;
@property (strong, nonatomic) IBOutlet UIButton *uiBtn_earnIncome_no;

- (IBAction)btn_abtUniversity:(id)sender;
- (IBAction)btn_StdntStatus:(id)sender;
- (IBAction)btn_abtAgeGrp:(id)sender;
- (IBAction)btn_abtGender:(id)sender;
- (IBAction)btn_earnIncome_yes:(id)sender;
- (IBAction)btn_earnIncome_no:(id)sender;
- (IBAction)btn_submit:(id)sender;

@end
