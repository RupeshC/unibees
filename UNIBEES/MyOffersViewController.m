//
//  MyOffersViewController.m
//  UNIBEES
//
//  Created by Rupesh Chakole on 23/07/16.
//  Copyright (c) 2016 Pradeep. All rights reserved.
//

#import "MyOffersViewController.h"
#import "DealsTableViewCell.h"
//#import "WToast.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "webservice.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <MapKit/MapKit.h>
#import "silentWebservice.h"
#import "MBProgressHUD.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>

@interface MyOffersViewController ()

@end

@implementation MyOffersViewController
{
    webservice *web;
    NSMutableArray *eventsArray;
    int cnt;
    int seletedIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
 
    self.tbl_MyOfferLst.estimatedRowHeight = 201.0;
    self.tbl_MyOfferLst.rowHeight = UITableViewAutomaticDimension;
    cnt=1;
    eventsArray=[[NSMutableArray alloc]init];
    web=[[webservice alloc]init];
    
    self.lbl_mssage.text=[NSString stringWithFormat:@"Bookmark some deals to see them here"];
    
    self.tbl_MyOfferLst.estimatedRowHeight = 100.0;
    self.tbl_MyOfferLst.rowHeight = UITableViewAutomaticDimension;
    cnt=1;
    eventsArray=[[NSMutableArray alloc]init];
    
    
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
 
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@",appDelegate.user.EMAIL_ID];
    web=[webservice alloc];
    web.owner=self;
    web.delegate=self;
    web.tag=1;
    [web postData:@"bookmarkdeal.php" para:myRequestString];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([eventsArray count])
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    [self.tbl_MyOfferLst reloadData];
}


#pragma mark - webservice delegate

//webservice delegate
- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess{
    
    if (web.tag==1)
    {
        if (web)
        {
            web.delegate=nil;
            web=nil;
            
        }
        
        if (sucess)
        {
            if (result)
            {
                NSLog(@"result %@",result);
                
                if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
                {
                    
                    eventsArray=[[NSMutableArray alloc]initWithArray:[result objectForKey:@"DEAL"]];
                    [self.tbl_MyOfferLst reloadData];
                    
                }
                else
                {
                    //[self ShowToast:@"Something Wrong"];
                }
                
                
                
            }
            else
            {
                [self ShowToast:@"Server Error"];
                
            }
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
        }
    }
    else
    {
        
        if (web)
        {
            web.delegate=nil;
            web=nil;
            
        }
        
        if (sucess)
        {
            if (result)
            {
                NSLog(@"result %@",result);
                
                if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
                {
                    
                    NSMutableDictionary *dic=[eventsArray objectAtIndex:seletedIndex];
                    if (![[dic valueForKey:@"DEAL_NUM_LIKES"] boolValue]) {
                        [dic removeObjectForKey:@"DEAL_NUM_LIKES"];
                        [dic setObject:@"1" forKey:@"DEAL_NUM_LIKES"];
                        
                    }
                    else
                    {
                        [dic removeObjectForKey:@"DEAL_NUM_LIKES"];
                        [dic setObject:@"0" forKey:@"DEAL_NUM_LIKES"];
                    }
                    
                    [eventsArray replaceObjectAtIndex:seletedIndex withObject:dic];
                    
                    [self.tbl_MyOfferLst reloadData];
                }
                else
                {
                    //[self ShowToast:@"Something Wrong"];
                }
                
                
                
            }
            else
            {
                [self ShowToast:@"Server Error"];
                
            }
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
        }
        
    }
}



#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    if ([eventsArray count]) {
        self.lbl_mssage.text=@"";
    }
    return [eventsArray count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    DealsTableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"DealsTableViewCell"];
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"DealsTableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
    
    cell.backgroundColor=[UIColor groupTableViewBackgroundColor];
    NSDictionary *dic=[eventsArray objectAtIndex:indexPath.row];
    cell.title.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_TITLE"]];
    
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    cell.time.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ To %@",[commanClass convertTimspanTodate:[dic valueForKey:@"DEAL_START_TIME"]],[commanClass convertTimspanTodate:[dic valueForKey:@"DEAL_END_TIME"]]]
                                                               attributes:underlineAttribute];
    
    UITapGestureRecognizer *dategesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(calendarTapGestureTap:)];
    // setup gesture as needed
    cell.time.userInteractionEnabled=true;
    cell.time.tag=1000+indexPath.row;
    [cell.time addGestureRecognizer:dategesture];
    
    cell.descprtion.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_COMMENT"]];
    
    
    cell.address.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_LOCATION_NAME"]]
                                                                  attributes:underlineAttribute];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressTapGestureTap:)];
    // setup gesture as needed
    cell.address.userInteractionEnabled=true;
    cell.address.tag=1000+indexPath.row;
    [cell.address addGestureRecognizer:gesture];
    
    cell.notes.text=@"";
    if ([commanClass daysDifference:[dic valueForKey:@"DEAL_START_TIME"]]>=0)
    {
        if ([commanClass daysDifference:[dic valueForKey:@"DEAL_START_TIME"]]<=1)
        {
            cell.notes.text=[NSString stringWithFormat:@"Deal starts in %d day",[commanClass daysDifference:[dic valueForKey:@"DEAL_START_TIME"]]];
        }
        else
        {
            cell.notes.text=[NSString stringWithFormat:@"Deal starts in %d days",[commanClass daysDifference:[dic valueForKey:@"DEAL_START_TIME"]]];
        }
        
    }

    
    //cell.notes.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_NOTES"]];
    [cell.img setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"DEAL_PHOTO_LINK"]] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    
    
    cell.thumb.tag=1000+indexPath.row;
    if ([[dic valueForKey:@"DEAL_NUM_LIKES"] boolValue])
    {
        
        [cell.thumb setImage:[UIImage imageNamed:@"thumbsup_green.png"] forState:UIControlStateNormal];
    }
    else
    {
        [cell.thumb setImage:[UIImage imageNamed:@"thumbsup_grey.png"] forState:UIControlStateNormal];
    }
    [cell.thumb addTarget:self action:@selector(bookMark:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (action==EKEventEditViewActionSaved)
        {
            
            [self ShowToast:@"Event created successfully"];
        }
        
    }];
    
    
}

- (IBAction)calendarTapGestureTap:(UITapGestureRecognizer *)sender {
    
    NSInteger index=[sender.view tag]-1000;
    
    NSDictionary *dic=[eventsArray objectAtIndex:index];
    
    EKEventStore *store = [[EKEventStore alloc] init];
    
    if([store respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        // iOS 6
        [store requestAccessToEntityType:EKEntityTypeEvent
                              completion:^(BOOL granted, NSError *error) {
                                  if (granted)
                                  {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [self createEventAndPresentViewController:store details:dic];
                                      });
                                  }
                              }];
    } else
    {
        // iOS 5
        [self createEventAndPresentViewController:store details:dic];
    }
}

- (void)createEventAndPresentViewController:(EKEventStore *)store details:(NSDictionary *)dic
{
    
    EKEvent *event = [EKEvent eventWithEventStore:store];
    
    event.title = [NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_TITLE"]];
    event.notes = [NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_COMMENT"]];
    event.location = [NSString stringWithFormat:@"%@",[dic valueForKey:@"DEAL_LOCATION_NAME"]];
    event.calendar = [store defaultCalendarForNewEvents];
    
    
    event.startDate = [commanClass getdate:[dic valueForKey:@"DEAL_START_TIME"]];
    
    event.endDate = [commanClass getdate:[dic valueForKey:@"DEAL_END_TIME"]];
    
    EKEventEditViewController *controller = [[EKEventEditViewController alloc] init];
    controller.event = event;
    controller.eventStore = store;
    controller.editViewDelegate = self;
    
    [self presentViewController:controller animated:YES completion:nil];
}
- (IBAction)addressTapGestureTap:(UITapGestureRecognizer *)sender {
    
    NSInteger index=[sender.view tag]-1000;
    
    NSDictionary *dic=[eventsArray objectAtIndex:index];
    
    
    NSString *nativeMapScheme = @"maps.apple.com";
    NSString* url = [NSString stringWithFormat:@"http://%@/maps?q=%f,%f", nativeMapScheme, [[dic valueForKey:@"DEAL_LOCATION_LAT"] doubleValue] ,[[dic valueForKey:@"DEAL_LOCATION_LONG"] doubleValue]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    
    
}

-(void)bookMark:(UIButton *)sender
{
    seletedIndex=[sender tag]-1000;
    
    NSDictionary *dic=[eventsArray objectAtIndex:seletedIndex];
    NSString *DealID=[dic valueForKey:@"DEAL_ID"];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
  
    NSString *bookpage=@"B2";
    
       if (![[dic valueForKey:@"DEAL_NUM_LIKES"] boolValue]) {
        
        //[sender setImage:[UIImage imageNamed:@"thumbsup_green.png"] forState:UIControlStateNormal];
        
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"USER_ID=%@&DEAL_ID=%@&BOOKMARK_TYPE=%@",appDelegate.user.USER_ID,DealID,bookpage];
        
        web=[webservice alloc];
        web.owner=self;
        web.delegate=self;
        web.tag=2;
        [web postData:@"bookmark.php" para:myRequestString];
        
    }
    else
    {
        AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        
        //  [sender setImage:[UIImage imageNamed:@"thumbsup_grey.png"] forState:UIControlStateNormal];
        
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@&DEAL_ID=%@&BOOKMARK_TYPE=%@",appDelegate.user.EMAIL_ID,DealID,bookpage];
        web=[webservice alloc];
        web.owner=self;
        web.delegate=self;
        web.tag=2;
        [web postData:@"unbookmark.php" para:myRequestString];
    }
    
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected %ld row", (long)indexPath.row);
}



- (IBAction)menu:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) ShowToast:(NSString *)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}
@end
