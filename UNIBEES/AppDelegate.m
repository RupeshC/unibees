//
//  AppDelegate.m
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "AppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "silentWebservice.h"
#import "pushnotificationViewController.h"
#import "TodaysEventViewController.h"
#import "FMDatabase.h"
@interface AppDelegate ()

@end

@implementation AppDelegate
{

}
@synthesize emailId,userID;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    [FBLoginView class];
    [FBProfilePictureView class];
    [FBAppEvents activateApp];
    
    [self registerForPushNotification];
    
  
    
    NSDictionary *remoteNotif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if(remoteNotif)
    {
        
        [self movetoPushscreen:remoteNotif];
    }
    
    UILocalNotification *localNotif =
    [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (localNotif) {
        
        [self movetoTodayEvetnscreen];
        
    }
    
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"setlocalNotification"])
    {
        [self setLocalNotificaiton];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"setlocalNotification"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    NSArray *arrayOfLocalNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications] ;
    
    for (UILocalNotification *localNotification in arrayOfLocalNotifications) {
        
        
        NSLog(@"the notification this is canceld is %@", localNotification.alertBody);
    }
    
    return YES;
}
-(void)registerForPushNotification
{
    // Checking if app is running iOS 8
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        // Register device for iOS8
        UIUserNotificationSettings *notificationSettings = [UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:notificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    } else {
        // Register device for iOS7
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeBadge];
    }
}
-(void)movetoPushscreen : (NSDictionary *)dic
{
    // {"ADHOC_ID":"23","ADHOC_TITLE":"title","ADHOC_DESCRIPTION":"mess"}
    
     NSLog(@"movetoPushscreen %@",dic);
     NSLog(@"movetoPushscreen %@",[[[dic objectForKey:@"aps"] objectForKey:@"message"] objectForKey:@"ADHOC"]);
    
    //****Just create ADHOC Dic from notification dic.
    NSDictionary *adHocDic=[[[dic objectForKey:@"aps"] objectForKey:@"message"] objectForKey:@"ADHOC"];
    
    [self CreateTable];
    if (adHocDic) {
      [self insertAdHocData:adHocDic];
    }
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    pushnotificationViewController *controller = (pushnotificationViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"pushnotificationViewController"];
    if (adHocDic) {
        controller.notificaitonDic=[[NSMutableDictionary alloc]initWithDictionary:adHocDic];
    }
    [(UINavigationController *)self.window.rootViewController pushViewController:controller animated:YES];
}
-(void)movetoTodayEvetnscreen
{
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    TodaysEventViewController *controller = (TodaysEventViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"TodaysEventViewController"];
    [(UINavigationController *)self.window.rootViewController pushViewController:controller animated:YES];
    
    
    
}
- (void) application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"PUSH ERROR: %@", error);
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken{
    
  
    NSString * token = [NSString stringWithFormat:@"%@", deviceToken];
    //Format token as you need:
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
    token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
    
      NSLog(@"deviceToken: %@", token);
    if (self.user.EMAIL_ID) {
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@&GCMID=%@",[NSString stringWithFormat:@"%@",self.user.EMAIL_ID],token];
        silentWebservice * silentweb=[silentWebservice alloc];
        silentweb.delegate=nil;
        [silentweb postData:@"gcm_ios.php" para:myRequestString];
    }
  
}
-(void) CreateTable{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"CREATE TABLE IF NOT EXISTS adhocnotification (ADHOC_ID TEXT DEFAULT NULL,ADHOC_TITLE TEXT DEFAULT NULL,ADHOC_DESCRIPTION TEXT DEFAULT NULL,ADHOC_TIME TEXT DEFAULT NULL)"];
    [database close];
}
-(void)insertAdHocData:(NSDictionary *)dic
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"UNIBEES.sqlite"];
    NSString *adid=[NSString stringWithFormat:@"%@",[dic valueForKey:@"ADHOC_ID"]];
     NSString *adtitle=[NSString stringWithFormat:@"%@",[dic valueForKey:@"ADHOC_TITLE"]];
    NSString *addesp=[NSString stringWithFormat:@"%@",[dic valueForKey:@"ADHOC_DESCRIPTION"]];
    
     NSString *adtime=[NSString stringWithFormat:@"0"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"INSERT INTO adhocnotification (ADHOC_ID,ADHOC_TITLE,ADHOC_DESCRIPTION,ADHOC_TIME) VALUES (?, ?, ?, ?)",adid,adtitle, addesp,adtime, nil];
    [database close];
}

-(BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation{
    
    NSLog(@"return openURL=%d",[FBAppCall handleOpenURL:url sourceApplication:sourceApplication]);
    
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication];
}

-(void)application:(UIApplication *)app didReceiveRemoteNotification:(NSDictionary *)userInfo
{

    [self movetoPushscreen:userInfo];
}
- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification
{
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive)
    {
        // user has tapped notification
    }
    else
    {
         [self movetoTodayEvetnscreen];
    }

    
}

-(void)setLocalNotificaiton
{
    
        NSCalendar *calendar = [NSCalendar currentCalendar];
    
        NSDate *referenceDate = [NSDate date];
        
        // set components for time 8:00 a.m.
        
        NSDateComponents *componentsForFireDate = [calendar components:(NSCalendarUnitYear | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate: referenceDate];
        
        [componentsForFireDate setHour:8];
        [componentsForFireDate setMinute:0];
        [componentsForFireDate setSecond:0];
        
        NSDate *fireDateOfNotification = [calendar dateFromComponents:componentsForFireDate];
    
        // Create the notification
        
        UILocalNotification *notification = [[UILocalNotification alloc] init];
        notification.fireDate = fireDateOfNotification;
        notification.timeZone = [NSTimeZone localTimeZone];
        notification.alertBody = [NSString stringWithFormat: @"Check out today's events."];
        notification.alertAction = @"Check out today's events.";
        notification.repeatInterval= NSCalendarUnitDay;
        notification.soundName = UILocalNotificationDefaultSoundName;
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    NSArray *arrayOfLocalNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications] ;
    
    for (UILocalNotification *localNotification in arrayOfLocalNotifications) {
        
        
            NSLog(@"the notification this is canceld is %@", localNotification.alertBody);
            
           
     
        
    }
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
