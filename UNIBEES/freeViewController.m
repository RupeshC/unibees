//
//  freeViewController.m
//  UNIBEES
//
//  Created by Pradeep on 19/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "freeViewController.h"
#import "FreeTableViewCell.h"
//#import "WToast.h"
#import "MBProgressHUD.h"
#import "AppDelegate.h"
#import "webservice.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import <MapKit/MapKit.h>
#import "silentWebservice.h"
#import "MBProgressHUD.h"
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
@interface freeViewController ()

@end

@implementation freeViewController
{
    webservice *web;
    silentWebservice *silentweb;
    NSString *BookStatus;
    NSMutableArray *eventsArray;
    int seletedIndex;
    int cnt;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tableView.estimatedRowHeight = 100.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    cnt=1;
    eventsArray=[[NSMutableArray alloc]init];
    self.messageLbl.text=[NSString stringWithFormat:@"No Events available"];
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"PAGERECORD=%i",cnt];
    web=[webservice alloc];
    web.owner=self;
    web.delegate=self;
    web.tag=1;
    [web postData:@"eventuser.php" para:myRequestString];
}
-(void)viewDidAppear:(BOOL)animated
{
    if ([eventsArray count])
    {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }
    [self.tableView reloadData];
}
-(void)loadNextPageInSilent
{
    cnt++;
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"PAGERECORD=%i",cnt];
    silentweb=[silentWebservice alloc];
    silentweb.delegate=self;
    [silentweb postData:@"eventuser.php" para:myRequestString];
    
}


#pragma mark - webservice delegate

//webservice delegate
- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess{
    
    if (web.tag==1)
    {
    if (web)
    {
        web.delegate=nil;
        web=nil;
        
    }
    
    if (sucess)
    {
        if (result)
        {
            NSLog(@"result %@",result);
            
            if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
            {
                
                eventsArray=[[NSMutableArray alloc]initWithArray:[result objectForKey:@"EVENT"]];
                [self.tableView reloadData];
                
        
                if ([[result objectForKey:@"NUMBER_OF_PAGES"] integerValue]>cnt)
                {
                    [self loadNextPageInSilent];
                    
                }
            }
            else
            {
                //[self ShowToast:@"Something Wrong"];
            }
            
            
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
            
        }
        
    }
    else
    {
        [self ShowToast:@"Server Error"];
    }
    }
    else
    {
        if (web)
        {
            web.delegate=nil;
            web=nil;
            
        }
        
        if (sucess)
        {
            if (result)
            {
                NSLog(@"result %@",result);
                
                if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
                {
                    NSMutableDictionary *dic=[eventsArray objectAtIndex:seletedIndex];
                    if (![[dic valueForKey:@"EVENT_NUM_LIKES"] boolValue]) {
                        [dic removeObjectForKey:@"EVENT_NUM_LIKES"];
                        [dic setObject:@"1" forKey:@"EVENT_NUM_LIKES"];
                        
                    }
                    else
                    {
                        [dic removeObjectForKey:@"EVENT_NUM_LIKES"];
                        [dic setObject:@"0" forKey:@"EVENT_NUM_LIKES"];
                    }
                    
                    [eventsArray replaceObjectAtIndex:seletedIndex withObject:dic];
                    
                    [self.tableView reloadData];
                   
                }
                else
                {
                    //[self ShowToast:@"Something Wrong"];
                }
                
                
                
            }
            else
            {
                [self ShowToast:@"Server Error"];
                
            }
            
        }
        else
        {
            [self ShowToast:@"Server Error"];
        }
    }
    
}

#pragma mark - silentwebservice delegate

- (void)finishsilentLoading:(NSDictionary *)result sucess:(BOOL)sucess
{
    
        if (silentweb)
        {
            silentweb.delegate=nil;
            silentweb=nil;
            
        }
        
        if (sucess)
        {
            if (result)
            {
                NSLog(@"result %@",result);
                
                if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
                {
                    
                    NSMutableArray *tempevent=[[NSMutableArray alloc]initWithArray:[result objectForKey:@"EVENT"]];
                    
                    for (NSDictionary *dic in tempevent)
                    {
                        [eventsArray addObject:dic];
                    }
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.tableView reloadData];
                    });
                    
                    if ([[result objectForKey:@"NUMBER_OF_PAGES"] integerValue]>cnt)
                    {
                        [self loadNextPageInSilent];
                        
                    }
                }
                
                
            }
            
            
        }
    
   

}


#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    if ([eventsArray count]) {
    self.messageLbl.text=@"";
    }
    return [eventsArray count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FreeTableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:@"FreeTableViewCell"];
        
        
    if (cell == nil) {
        // Load the top-level objects from the custom cell XIB.
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"FreeTableViewCell" owner:self options:nil];
        // Grab a pointer to the first object (presumably the custom cell, as that's all the XIB should contain).
        cell = [topLevelObjects objectAtIndex:0];
    }
        
    cell.backgroundColor=[UIColor groupTableViewBackgroundColor];
    NSDictionary *dic=[eventsArray objectAtIndex:indexPath.row];
    cell.title.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_TITLE"]];
    
    cell.thumb.hidden=true;
   
    NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    cell.time.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ To %@",[commanClass convertTimspanTodate:[dic valueForKey:@"EVENT_START_TIME"]],[commanClass convertTimspanTodate:[dic valueForKey:@"EVENT_END_TIME"]]]
                                                             attributes:underlineAttribute];

    UITapGestureRecognizer *dategesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(calendarTapGestureTap:)];
    // setup gesture as needed
    cell.time.userInteractionEnabled=true;
    cell.time.tag=1000+indexPath.row;
    [cell.time addGestureRecognizer:dategesture];
    
    cell.descprtion.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_COMMENT"]];
    
 
    cell.address.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_LOCATION_NAME"]]
                                                               attributes:underlineAttribute];
    UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addressTapGestureTap:)];
    // setup gesture as needed
    cell.address.userInteractionEnabled=true;
    cell.address.tag=1000+indexPath.row;
    [cell.address addGestureRecognizer:gesture];

    cell.notes.text=[NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_NOTES"]];
    [cell.img setImageWithURL:[NSURL URLWithString:[dic valueForKey:@"EVENT_PHOTO_LINK"]] placeholderImage:[UIImage imageNamed:@"placeholder.jpg"] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    NSMutableArray *catrgry=[[NSMutableArray alloc]init];
    
    if ([[dic valueForKey:@"FREE_FOOD"] boolValue])
    {
        [catrgry addObject:[UIImage imageNamed:@"free_food.png"]];
    }
    if ([[dic valueForKey:@"FREE_GAMES"] boolValue])
    {
        [catrgry addObject:[UIImage imageNamed:@"free_games.png"]];
    }
    if ([[dic valueForKey:@"FREE_GOODIES"] boolValue])
    {
        [catrgry addObject:[UIImage imageNamed:@"free_goodies.png"]];
    }
    if ([[dic valueForKey:@"FREE_INFO"] boolValue])
    {
        [catrgry addObject:[UIImage imageNamed:@"free_info.png"]];
    }
    if ([[dic valueForKey:@"PAID_EVENT"] boolValue])
    {
        [catrgry addObject:[UIImage imageNamed:@"paid_events.png"]];
    }
    
    for (int i=1; i<=5; i++) {
        if ([[cell viewWithTag:i] class]==[UIImageView class])
        {
        UIImageView *img=[cell viewWithTag:i];
        img.hidden=true;
        }
    }
   
    
    
        for (int i=1; i<=[catrgry count]; i++) {
            if ([[cell viewWithTag:i] class]==[UIImageView class])
            {
            UIImageView *img=[cell viewWithTag:i];
            img.hidden=false;
            img.image=catrgry[i-1];
            }
        }
   
    
    
    cell.thumb.tag=1000+indexPath.row;
    if ([[dic valueForKey:@"EVENT_NUM_LIKES"] boolValue])
    {
        BookStatus=@"unbook";
        [cell.thumb setImage:[UIImage imageNamed:@"thumbsup_green.png"] forState:UIControlStateNormal];
    }
    else
    {
        BookStatus=@"book";
        [cell.thumb setImage:[UIImage imageNamed:@"thumbsup_grey.png"] forState:UIControlStateNormal];
    }
    [cell.thumb addTarget:self action:@selector(bookMark:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
    
}
- (void)eventEditViewController:(EKEventEditViewController *)controller didCompleteWithAction:(EKEventEditViewAction)action
{
    
    [self dismissViewControllerAnimated:YES completion:^{
        if (action==EKEventEditViewActionSaved)
        {
            
            [self ShowToast:@"Event created successfully"];
        }

    }];
        
    
}

- (IBAction)calendarTapGestureTap:(UITapGestureRecognizer *)sender {
   
    NSInteger index=[sender.view tag]-1000;
    
    NSDictionary *dic=[eventsArray objectAtIndex:index];
    
    EKEventStore *store = [[EKEventStore alloc] init];
    
    if([store respondsToSelector:@selector(requestAccessToEntityType:completion:)])
    {
        // iOS 6
        [store requestAccessToEntityType:EKEntityTypeEvent
                              completion:^(BOOL granted, NSError *error) {
                                  if (granted)
                                  {
                                      dispatch_async(dispatch_get_main_queue(), ^{
                                          [self createEventAndPresentViewController:store details:dic];
                                      });
                                  }
                              }];
    } else
    {
        // iOS 5
        [self createEventAndPresentViewController:store details:dic];
    }
}

- (void)createEventAndPresentViewController:(EKEventStore *)store details:(NSDictionary *)dic
{
   
    EKEvent *event = [EKEvent eventWithEventStore:store];
    
    event.title = [NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_TITLE"]];
    event.notes = [NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_COMMENT"]];
    event.location = [NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_LOCATION_NAME"]];
    event.calendar = [store defaultCalendarForNewEvents];
    
    
    event.startDate = [commanClass getdate:[dic valueForKey:@"EVENT_START_TIME"]];
    
    event.endDate = [commanClass getdate:[dic valueForKey:@"EVENT_END_TIME"]];
    
    EKEventEditViewController *controller = [[EKEventEditViewController alloc] init];
    controller.event = event;
    controller.eventStore = store;
    controller.editViewDelegate = self;
    
    [self presentViewController:controller animated:YES completion:nil];
}
- (IBAction)addressTapGestureTap:(UITapGestureRecognizer *)sender {
   
    NSInteger index=[sender.view tag]-1000;
    
    NSDictionary *dic=[eventsArray objectAtIndex:index];
    
    
    NSString *nativeMapScheme = @"maps.apple.com";
    NSString* url = [NSString stringWithFormat:@"http://%@/maps?q=%f,%f", nativeMapScheme, [[dic valueForKey:@"EVENT_LOCATION_LAT"] doubleValue] ,[[dic valueForKey:@"EVENT_LOCATION_LONG"] doubleValue]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];

   
}

-(void)bookMark:(UIButton *)sender
{
    
      seletedIndex=[sender tag]-1000;
    
    NSDictionary *dic=[eventsArray objectAtIndex:seletedIndex];
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    NSString *EventID=[NSString stringWithFormat:@"%@",[dic valueForKey:@"EVENT_ID"]];
    
    NSString *UserID=appDelegate.user.USER_ID;
    
    
   
    NSString *bookpage=@"B1";
    


    
    if (![[dic valueForKey:@"EVENT_NUM_LIKES"] boolValue]) {
        
        //[sender setImage:[UIImage imageNamed:@"thumbsup_green.png"] forState:UIControlStateNormal];
        
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"USER_ID=%@&BOOKMARK_TYPE=%@&EVENT_ID=%@",UserID,bookpage,EventID];
        
        web=[webservice alloc];
        web.owner=self;
        web.delegate=self;
        web.tag=2;
        [web postData:@"bookmark.php" para:myRequestString];
        
    }
    else
    {
        
        //[sender setImage:[UIImage imageNamed:@"thumbsup_grey.png"] forState:UIControlStateNormal];
        
        NSString *myRequestString = [[NSString alloc] initWithFormat:@"EMAIL_ID=%@&BOOKMARK_TYPE=%@&EVENT_ID=%@",appDelegate.user.EMAIL_ID,bookpage,EventID];
        
        web=[webservice alloc];
        web.owner=self;
        web.delegate=self;
        web.tag=2;
        [web postData:@"unbookmark.php" para:myRequestString];
    }

    
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"selected %ld row", (long)indexPath.row);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)menu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void) ShowToast:(NSString *)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}


@end
