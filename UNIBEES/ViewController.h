//
//  ViewController.h
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface ViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>
- (IBAction)login:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *email;

@property (weak, nonatomic) IBOutlet UITextField *pass;
- (IBAction)btn_RegisterByMail:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *UIBtn_regByMail;
@property (strong, nonatomic) IBOutlet UIButton *UILoginBtn;

@property (strong, nonatomic) IBOutlet FBLoginView *FBLoginBtn;


@end

