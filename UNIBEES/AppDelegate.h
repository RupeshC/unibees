//
//  AppDelegate.h
//  UNIBEES
//
//  Created by Pradeep on 17/07/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "user.h"
#import <FacebookSDK/FacebookSDK.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) FBLoginView *FBLoginBtn;
@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) user *user;

@property (strong, nonatomic) NSString *emailId,*userID;
@end

