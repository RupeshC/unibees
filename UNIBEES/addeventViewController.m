//
//  addeventViewController.m
//  UNIBEES
//
//  Created by HItesh on 7/25/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import "addeventViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "webservice.h"
//#include "WToast.h"
#include "ViewController.h"
#include "HomeViewController.h"
#include "MBProgressHUD.h"


@interface addeventViewController ()

@end

@implementation addeventViewController
{
    webservice *web;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:NO];
    [self.txt_eventTitle setDelegate:self];
    [self.txt_eventLocation setDelegate:self];
    [self.txtfld_eventDiscription setDelegate:self];
 
    _submit.layer.cornerRadius = 10; // this value vary as per your desire
    _submit.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


//TextView Delegates

-(void)textViewDidBeginEditing:(UITextView *)textView:(UITextField *)textField
{
}


-(void)textViewDidBeginEditing:(UITextView *)textView{
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"])
    {
        [textView resignFirstResponder];
    }
    return YES;
}


-(void)textViewDidEndEditing:(UITextView *)textView:(UITextField *)textField
{
}
//========





// TextField Delegates
-(void)textFieldDidBeginEditing:(UITextField *)textField
{
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (IBAction)menu:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btn_submit:(id)sender {
    
    NSString *myRequestString = [[NSString alloc] initWithFormat:@"ADH_TITLE=%@&ADH_DESC=%@&ADH_LOCATION_NAME=%@&ADH_START_TIME=%@&ADH_END_TIME=%@",_txt_eventTitle.text,_txtfld_eventDiscription.text,_txt_eventLocation.text,@"14038320388",@"14038320389"];
    
    
    web=[webservice alloc];
    web.owner=self;
    web.delegate=self;
    
    [web postData:@"adhoc.php" para:myRequestString];
}


//webservice delegate
- (void)finishLoading:(NSDictionary *)result sucess:(BOOL)sucess{
    if (web)
    {
        web.delegate=nil;
        web=nil;
    }
    
    if (sucess)
    {
        if (result)
        {
            NSLog(@"result %@",result);
            
            if ([[result objectForKey:@"RESPONSE_STATUS"] integerValue]==1)
            {
                [self ShowToast:@"Event added successfully."];
                HomeViewController *controller = (HomeViewController*)[self.storyboard instantiateViewControllerWithIdentifier: @"HomeViewController"];
                [self.navigationController pushViewController:controller animated:YES];
            }
            else
            {
                [self ShowToast:@"Something went wrong"];
            }
        }
        else
        {
            [self ShowToast:@"Server Error"];
            
        }
        
    }
    else
    {
        [self ShowToast:@"Server Error"];
    }
}


-(void) ShowToast:(NSString *)msg{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
    
    // Configure for text only and offset down
    hud.mode = MBProgressHUDModeText;
    hud.label.text = msg;
    hud.margin = 10.f;
    hud.yOffset = 150.f;
    hud.removeFromSuperViewOnHide = YES;
    
    [hud hideAnimated:YES afterDelay:3];
}


@end
