//
//  addeventViewController.h
//  UNIBEES
//
//  Created by HItesh on 7/25/16.
//  Copyright © 2016 Pradeep. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface addeventViewController : UIViewController
- (IBAction)menu:(id)sender;
@property (strong, nonatomic) IBOutlet UITextField *txt_eventTitle;
@property (strong, nonatomic) IBOutlet UITextView *txtfld_eventDiscription;
@property (strong, nonatomic) IBOutlet UITextField *txt_eventLocation;
- (IBAction)btn_submit:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *submit;

@end
